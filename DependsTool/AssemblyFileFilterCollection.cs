﻿using System.Collections.Generic;
using System.Linq;

namespace DependsTool
{
	class AssemblyFileFilterCollection : AssemblyFileFilter
	{
		private readonly List<AssemblyFileFilter> list = new List<AssemblyFileFilter>();

		public AssemblyFileFilterCollection(List<AssemblyFileFilter> list)
		{
			this.list.AddRange(list);
		}

		public void Add(AssemblyFileFilter filter)
		{
			list.Add(filter);
		}

		public override bool Include(AssemblyFile assemblyFile)
		{
			return
				base.Include(assemblyFile) &&
				list.Any(f => f.Include(assemblyFile));
		}
	}
}
