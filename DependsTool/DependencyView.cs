﻿using System;
using System.Windows.Forms;

namespace DependsTool
{
	public partial class DependencyView : UserControl
	{
		private AssemblyFileRepository repository;
		private AssemblyFile assemblyFile;

		private bool isListLoaded;
		private bool isTreeLoaded;

		public DependencyView()
		{
			InitializeComponent();
			listView.Dock = DockStyle.Fill;
			treeView.Dock = DockStyle.Fill;
			buttonView.Text = menuItemList.Text;
		}

		public string Title
		{
			get
			{
				return ItemName() + " - Dependency";
			}
		}

		public void LoadData(AssemblyFileRepository repository, AssemblyFile assemblyFile)
		{
			this.repository = repository;
			this.assemblyFile = assemblyFile;

			if (listView.Visible)
			{
				listView.LoadData(repository, assemblyFile);
				isListLoaded = true;
			}

			if (treeView.Visible)
			{
				treeView.LoadData(repository, assemblyFile);
				isTreeLoaded = true;
			}
		}


		private string ItemName()
		{
			if (assemblyFile != null)
			{
				return assemblyFile.ShortName;
			}
			else
			{
				return "(unknown)";
			}
		}

		private void listToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SetView(true);
		}


		private void treeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SetView(false);
		}

		private void SetView(bool isList)
		{
			listView.Visible = isList;
			treeView.Visible = !isList;

			if (isList && !isListLoaded)
			{
				listView.LoadData(repository, assemblyFile);
			}

			if (!isList && !isTreeLoaded)
			{
				treeView.LoadData(repository, assemblyFile);
			}
		}

		private void menuItemList_Click(object sender, EventArgs e)
		{
			buttonView.Text = menuItemList.Text;
			SetView(true);
		}

		private void menuItemTree_Click(object sender, EventArgs e)
		{
			buttonView.Text = menuItemTree.Text;
			SetView(false);
		}
	}
}
