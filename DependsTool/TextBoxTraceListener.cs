﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace DependsTool
{
	/// <summary>
	/// The TextBoxTraceListener class implements a trace listener that outputs to a System.Windows.Forms.TextBox control.
	/// </summary>
	internal class TextBoxTraceListener : TraceListener
	{
		private TextBox textBox;
		private delegate void WriteDelegate(string message);

		public TextBoxTraceListener(TextBox textBox)
			: base()
		{
			this.textBox = textBox;
		}

		public TextBoxTraceListener(string name, TextBox textBox)
			: base(name)
		{
			this.textBox = textBox;
		}


		public override void Write(string message)
		{
			if (textBox.InvokeRequired)
			{
				textBox.Invoke(new WriteDelegate(DoWrite), message);
			}
			else
			{
				DoWrite(message);
			}
		}

		public override void WriteLine(string message)
		{
			Write(message + Environment.NewLine);
		}

		private void DoWrite(string message)
		{
			textBox.AppendText(message);
		}
	}
}