﻿using System;
using System.Reflection;
using System.Text;

namespace DependsTool
{
    internal static class ExceptionExtensions
    {
        public static string ToStringFull(this Exception ex)
        {
            StringBuilder sb = new StringBuilder();

            int level = 0;

            while (ex != null)
            {
                sb.AppendLine("--Level " + level.ToString() + "--");
                sb.AppendLine("Type: " + ex.GetType().ToString());
                sb.AppendLine("Message: " + ex.Message);
                sb.AppendLine("Source: " + ex.Source);
                sb.AppendLine("StackTrace:" + ex.StackTrace);
                level++;
                ex = ex.InnerException;
            }

            return sb.ToString();
        }

        public static string ToStringStandard(this Exception ex)
        {
            StringBuilder sb = new StringBuilder();

            AppendExceptionMessage(sb, ex);
            sb.AppendLine();
            AppendStackTrace(sb, ex);

            if (ex is ReflectionTypeLoadException parentLoadException)
            {
                int index = 0;
                foreach (var loaderException in parentLoadException.LoaderExceptions)
                {
                    sb.AppendLine($"LOADER EXCEPTION {index}------------------------------");
                    sb.AppendLine(loaderException.ToStringStandard());
                    index++;
                }
            }

            return sb.ToString();
        }

        private static void AppendExceptionMessage(StringBuilder sb, Exception ex)
        {
            while (ex != null)
            {
                sb.Append(ex.GetType().FullName + ": " + ex.Message);
                ex = ex.InnerException;
                if (ex != null)
                {
                    sb.Append(" ---> ");
                }
            }
        }

        private static void AppendStackTrace(StringBuilder sb, Exception ex)
        {
            if (ex.InnerException != null)
            {
                AppendStackTrace(sb, ex.InnerException);
                sb.AppendLine("   --- End of inner exception stack trace ---");
            }
            sb.Append(ex.StackTrace);
        }
    }
}
