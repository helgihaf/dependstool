﻿using System;

namespace DependsTool
{
	internal abstract class AssemblyFileFilter
	{
		public string Text { get; set; }

		public bool Referenced { get; set; }
		public bool Unreferenced { get; set; }

		public virtual bool Include(AssemblyFile assemblyFile)
		{
			return
				Referenced == Unreferenced == true
				||
				Referenced && assemblyFile.ReferencedBy.Count > 0
				||
				Unreferenced && assemblyFile.ReferencedBy.Count == 0;
		}

		protected bool IncludeString(string target)
		{
			return target != null && target.IndexOf(Text, StringComparison.CurrentCultureIgnoreCase) >= 0;
		}
	}
}
