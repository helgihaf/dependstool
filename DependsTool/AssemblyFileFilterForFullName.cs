﻿namespace DependsTool
{
	class AssemblyFileFilterForFullName : AssemblyFileFilter
	{
		public override bool Include(AssemblyFile assemblyFile)
		{
			return
				base.Include(assemblyFile) &&
				IncludeString(assemblyFile.FullName);
		}
	}
}
