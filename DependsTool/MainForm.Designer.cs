﻿namespace DependsTool
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.buttonBrowse = new System.Windows.Forms.Button();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonScan = new System.Windows.Forms.Button();
			this.comboBoxFolder = new System.Windows.Forms.ComboBox();
			this.tabControl = new System.Windows.Forms.TabControl();
			this.tabPageAssemblies = new System.Windows.Forms.TabPage();
			this.explorer = new DependsTool.AssemblyFileExplorer();
			this.tabPageMessages = new System.Windows.Forms.TabPage();
			this.textBoxMessages = new System.Windows.Forms.TextBox();
			this.tabPageGraph = new System.Windows.Forms.TabPage();
			this.label2 = new System.Windows.Forms.Label();
			this.comboBoxRenderer = new System.Windows.Forms.ComboBox();
			this.checkBoxIncludeVersion = new System.Windows.Forms.CheckBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.checkBoxVsHost = new System.Windows.Forms.CheckBox();
			this.checkBoxFramework = new System.Windows.Forms.CheckBox();
			this.buttonGenerate = new System.Windows.Forms.Button();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.groupBox1.SuspendLayout();
			this.tabControl.SuspendLayout();
			this.tabPageAssemblies.SuspendLayout();
			this.tabPageMessages.SuspendLayout();
			this.tabPageGraph.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.buttonBrowse);
			this.groupBox1.Controls.Add(this.progressBar);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.buttonScan);
			this.groupBox1.Controls.Add(this.comboBoxFolder);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(6);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(6);
			this.groupBox1.Size = new System.Drawing.Size(2528, 146);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			// 
			// buttonBrowse
			// 
			this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonBrowse.Location = new System.Drawing.Point(2294, 58);
			this.buttonBrowse.Margin = new System.Windows.Forms.Padding(6);
			this.buttonBrowse.Name = "buttonBrowse";
			this.buttonBrowse.Size = new System.Drawing.Size(48, 44);
			this.buttonBrowse.TabIndex = 2;
			this.buttonBrowse.Text = "...";
			this.buttonBrowse.UseVisualStyleBackColor = true;
			this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
			// 
			// progressBar
			// 
			this.progressBar.Location = new System.Drawing.Point(30, 112);
			this.progressBar.Margin = new System.Windows.Forms.Padding(6);
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(2312, 25);
			this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
			this.progressBar.TabIndex = 4;
			this.progressBar.Visible = false;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(24, 31);
			this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(155, 25);
			this.label1.TabIndex = 0;
			this.label1.Text = "Folder to scan:";
			// 
			// buttonScan
			// 
			this.buttonScan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonScan.Location = new System.Drawing.Point(2354, 58);
			this.buttonScan.Margin = new System.Windows.Forms.Padding(6);
			this.buttonScan.Name = "buttonScan";
			this.buttonScan.Size = new System.Drawing.Size(150, 44);
			this.buttonScan.TabIndex = 3;
			this.buttonScan.Text = "&Scan";
			this.buttonScan.UseVisualStyleBackColor = true;
			this.buttonScan.Click += new System.EventHandler(this.buttonScan_Click);
			// 
			// comboBoxFolder
			// 
			this.comboBoxFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxFolder.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.comboBoxFolder.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystemDirectories;
			this.comboBoxFolder.FormattingEnabled = true;
			this.comboBoxFolder.Location = new System.Drawing.Point(30, 62);
			this.comboBoxFolder.Margin = new System.Windows.Forms.Padding(6);
			this.comboBoxFolder.Name = "comboBoxFolder";
			this.comboBoxFolder.Size = new System.Drawing.Size(2248, 33);
			this.comboBoxFolder.TabIndex = 5;
			// 
			// tabControl
			// 
			this.tabControl.Controls.Add(this.tabPageAssemblies);
			this.tabControl.Controls.Add(this.tabPageMessages);
			this.tabControl.Controls.Add(this.tabPageGraph);
			this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl.Location = new System.Drawing.Point(0, 146);
			this.tabControl.Margin = new System.Windows.Forms.Padding(6);
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabControl.Size = new System.Drawing.Size(2528, 899);
			this.tabControl.TabIndex = 1;
			// 
			// tabPageAssemblies
			// 
			this.tabPageAssemblies.Controls.Add(this.explorer);
			this.tabPageAssemblies.Location = new System.Drawing.Point(4, 34);
			this.tabPageAssemblies.Margin = new System.Windows.Forms.Padding(6);
			this.tabPageAssemblies.Name = "tabPageAssemblies";
			this.tabPageAssemblies.Padding = new System.Windows.Forms.Padding(6);
			this.tabPageAssemblies.Size = new System.Drawing.Size(2520, 861);
			this.tabPageAssemblies.TabIndex = 0;
			this.tabPageAssemblies.Text = "Assemblies";
			this.tabPageAssemblies.UseVisualStyleBackColor = true;
			// 
			// explorer
			// 
			this.explorer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.explorer.Location = new System.Drawing.Point(6, 6);
			this.explorer.Margin = new System.Windows.Forms.Padding(12);
			this.explorer.Name = "explorer";
			this.explorer.Size = new System.Drawing.Size(2508, 849);
			this.explorer.TabIndex = 0;
			// 
			// tabPageMessages
			// 
			this.tabPageMessages.Controls.Add(this.textBoxMessages);
			this.tabPageMessages.Location = new System.Drawing.Point(4, 34);
			this.tabPageMessages.Margin = new System.Windows.Forms.Padding(6);
			this.tabPageMessages.Name = "tabPageMessages";
			this.tabPageMessages.Padding = new System.Windows.Forms.Padding(6);
			this.tabPageMessages.Size = new System.Drawing.Size(2520, 861);
			this.tabPageMessages.TabIndex = 1;
			this.tabPageMessages.Text = "Messages";
			this.tabPageMessages.UseVisualStyleBackColor = true;
			// 
			// textBoxMessages
			// 
			this.textBoxMessages.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxMessages.Location = new System.Drawing.Point(6, 6);
			this.textBoxMessages.Margin = new System.Windows.Forms.Padding(6);
			this.textBoxMessages.Multiline = true;
			this.textBoxMessages.Name = "textBoxMessages";
			this.textBoxMessages.ReadOnly = true;
			this.textBoxMessages.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.textBoxMessages.Size = new System.Drawing.Size(2508, 849);
			this.textBoxMessages.TabIndex = 0;
			// 
			// tabPageGraph
			// 
			this.tabPageGraph.Controls.Add(this.label2);
			this.tabPageGraph.Controls.Add(this.comboBoxRenderer);
			this.tabPageGraph.Controls.Add(this.checkBoxIncludeVersion);
			this.tabPageGraph.Controls.Add(this.panel1);
			this.tabPageGraph.Controls.Add(this.checkBoxVsHost);
			this.tabPageGraph.Controls.Add(this.checkBoxFramework);
			this.tabPageGraph.Controls.Add(this.buttonGenerate);
			this.tabPageGraph.Location = new System.Drawing.Point(4, 34);
			this.tabPageGraph.Margin = new System.Windows.Forms.Padding(6);
			this.tabPageGraph.Name = "tabPageGraph";
			this.tabPageGraph.Size = new System.Drawing.Size(2520, 861);
			this.tabPageGraph.TabIndex = 2;
			this.tabPageGraph.Text = "Dependency Graph";
			this.tabPageGraph.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(631, 24);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(101, 25);
			this.label2.TabIndex = 7;
			this.label2.Text = "Renderer";
			// 
			// comboBoxRenderer
			// 
			this.comboBoxRenderer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxRenderer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxRenderer.FormattingEnabled = true;
			this.comboBoxRenderer.Location = new System.Drawing.Point(636, 52);
			this.comboBoxRenderer.Name = "comboBoxRenderer";
			this.comboBoxRenderer.Size = new System.Drawing.Size(1858, 33);
			this.comboBoxRenderer.TabIndex = 6;
			// 
			// checkBoxIncludeVersion
			// 
			this.checkBoxIncludeVersion.AutoSize = true;
			this.checkBoxIncludeVersion.Location = new System.Drawing.Point(399, 23);
			this.checkBoxIncludeVersion.Name = "checkBoxIncludeVersion";
			this.checkBoxIncludeVersion.Size = new System.Drawing.Size(176, 29);
			this.checkBoxIncludeVersion.TabIndex = 5;
			this.checkBoxIncludeVersion.Text = "Include version";
			this.checkBoxIncludeVersion.UseVisualStyleBackColor = true;
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.AutoScroll = true;
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.pictureBox1);
			this.panel1.Location = new System.Drawing.Point(16, 167);
			this.panel1.Margin = new System.Windows.Forms.Padding(6);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(2478, 665);
			this.panel1.TabIndex = 4;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Margin = new System.Windows.Forms.Padding(6);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(637, 397);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox1.TabIndex = 3;
			this.pictureBox1.TabStop = false;
			// 
			// checkBoxVsHost
			// 
			this.checkBoxVsHost.AutoSize = true;
			this.checkBoxVsHost.Location = new System.Drawing.Point(10, 67);
			this.checkBoxVsHost.Margin = new System.Windows.Forms.Padding(6);
			this.checkBoxVsHost.Name = "checkBoxVsHost";
			this.checkBoxVsHost.Size = new System.Drawing.Size(330, 29);
			this.checkBoxVsHost.TabIndex = 2;
			this.checkBoxVsHost.Text = "Include .vshost.exe assemblies";
			this.checkBoxVsHost.UseVisualStyleBackColor = true;
			// 
			// checkBoxFramework
			// 
			this.checkBoxFramework.AutoSize = true;
			this.checkBoxFramework.Location = new System.Drawing.Point(10, 23);
			this.checkBoxFramework.Margin = new System.Windows.Forms.Padding(6);
			this.checkBoxFramework.Name = "checkBoxFramework";
			this.checkBoxFramework.Size = new System.Drawing.Size(380, 29);
			this.checkBoxFramework.TabIndex = 1;
			this.checkBoxFramework.Text = "Include .NET Framework assemblies";
			this.checkBoxFramework.UseVisualStyleBackColor = true;
			// 
			// buttonGenerate
			// 
			this.buttonGenerate.Enabled = false;
			this.buttonGenerate.Location = new System.Drawing.Point(16, 112);
			this.buttonGenerate.Margin = new System.Windows.Forms.Padding(6);
			this.buttonGenerate.Name = "buttonGenerate";
			this.buttonGenerate.Size = new System.Drawing.Size(150, 44);
			this.buttonGenerate.TabIndex = 0;
			this.buttonGenerate.Text = "Generate";
			this.buttonGenerate.UseVisualStyleBackColor = true;
			this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(2528, 1045);
			this.Controls.Add(this.tabControl);
			this.Controls.Add(this.groupBox1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(6);
			this.Name = "MainForm";
			this.Text = ".NET Assembly Dependency Tool";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.tabControl.ResumeLayout(false);
			this.tabPageAssemblies.ResumeLayout(false);
			this.tabPageMessages.ResumeLayout(false);
			this.tabPageMessages.PerformLayout();
			this.tabPageGraph.ResumeLayout(false);
			this.tabPageGraph.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button buttonScan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageAssemblies;
        private System.Windows.Forms.TabPage tabPageMessages;
        private System.Windows.Forms.TextBox textBoxMessages;
        private AssemblyFileExplorer explorer;
		private System.Windows.Forms.Button buttonBrowse;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.TabPage tabPageGraph;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.CheckBox checkBoxVsHost;
        private System.Windows.Forms.CheckBox checkBoxFramework;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox comboBoxFolder;
		private System.Windows.Forms.CheckBox checkBoxIncludeVersion;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox comboBoxRenderer;
	}
}

