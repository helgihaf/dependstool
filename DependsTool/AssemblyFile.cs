﻿using Mono.Cecil;
using System;
using System.Collections.Generic;

namespace DependsTool
{
	public class AssemblyFile
	{
		private string fullName;
		private AssemblyQuickInfo quickInfo;

		public AssemblyFile()
		{
			References = new List<AssemblyFile>();
			ReferencedBy = new List<AssemblyFile>();
		}

		/// <summary>
		/// Full file path of the assembly file or null if it was not found in the scan (but was created because it is referenced by onther assembly file).
		/// </summary>
		public string FilePath { get; set; }

		/// <summary>
		/// A ready-to-go assembly definition
		/// </summary>
		public AssemblyDefinition AssemblyDefinition { get; set; }

		/// <summary>
		/// List of assemblies that this assembly references.
		/// </summary>
		public List<AssemblyFile> References { get; private set; }

		/// <summary>
		/// List of assemblies that reference this assembly.
		/// </summary>
		public List<AssemblyFile> ReferencedBy { get; private set; }

		/// <summary>
		/// Display name for UI.
		/// </summary>
		public string DisplayName
		{
			get { return FullName; }
		}

		public string FullName
		{
			get
			{
				if (AssemblyDefinition != null)
				{
					return AssemblyDefinition.Name.FullName;
				}
				else
				{
					return fullName;
				}
			}

			set
			{
				if (AssemblyDefinition != null)
				{
					throw new InvalidOperationException("Cannot set full name when AssemblyDefinition is set");
				}
				fullName = value;
			}
		}

		public string ShortName
		{
			get
			{
				if (AssemblyDefinition != null)
				{
					return AssemblyDefinition.Name.Name;
				}
				else
				{
					int index = fullName.IndexOf(",");
					if (index >= 0)
					{
						return fullName.Substring(0, index);
					}
					return fullName;
				}
			}
		}

		public AssemblyQuickInfo QuickInfo
		{
			get
			{
				if (quickInfo == null)
				{
					quickInfo = new AssemblyQuickInfo(this);
				}
				return quickInfo;
			}
		}

		internal bool IsProcessed { get; set; }

		public bool IsDotNetFramework
		{
			get
			{
				var fn = FullName;
				return
					fn.IndexOf("System.", StringComparison.InvariantCulture) == 0
					|| fn.IndexOf("System,", StringComparison.InvariantCulture) == 0
					|| fn.IndexOf("mscorlib,", StringComparison.InvariantCulture) == 0
					;
			}
		}

		public bool IsVsHost
		{
			get
			{
				var fn = FullName;
				return
					fn.IndexOf("Microsoft.VisualStudio.HostinProcess.") == 0
					|| fn.IndexOf("vshost,") == 0
					;
			}
		}
	}
}
