﻿namespace DependsTool
{
    interface INodeBuilder
    {
        void Build(AssemblyFileFilter filter);
    }
}
