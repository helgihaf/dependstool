﻿namespace DependsTool
{
	public enum AssemblyArchitecture
	{
		Unknown,
		AnyCPU,
		I386,
		AMD64,
		IA64,
	}
}
