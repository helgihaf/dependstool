﻿using System.Linq;
using System.Windows.Forms;

namespace DependsTool
{
    internal class AssemblyFileNodeBuilder : INodeBuilder
    {
        private TreeView treeView;
        private AssemblyFileRepository repository;

        public AssemblyFileNodeBuilder(TreeView treeView, AssemblyFileRepository repository)
        {
            this.treeView = treeView;
            this.repository = repository;
        }

        public void Build(AssemblyFileFilter filter)
        {
            var assemblyFiles =
                from assemblyFile in repository.Values
                where (filter == null || filter.Include(assemblyFile))
                orderby assemblyFile.FullName
                select assemblyFile;

            foreach (var assemblyFile in assemblyFiles)
            {
                var assemblyNode = new TreeNode();
                SetTreeNode(assemblyFile, assemblyNode);

                var referencesNode = new TreeNode();
                referencesNode.Text = "References";
                referencesNode.ImageKey = "Folder";
                referencesNode.SelectedImageKey = referencesNode.ImageKey;
                BuildReferencesNodes(assemblyFile, referencesNode);
                assemblyNode.Nodes.Add(referencesNode);

                var referencedByNode = new TreeNode();
                referencedByNode.Text = "Referenced By";
                referencedByNode.ImageKey = "Folder";
                referencedByNode.SelectedImageKey = referencesNode.ImageKey;
                BuildReferencedByNode(assemblyFile, referencedByNode);
                assemblyNode.Nodes.Add(referencedByNode);

                var typesNode = new TreeNode();
                typesNode.Text = "Types";
                typesNode.ImageKey = "Folder";
                typesNode.SelectedImageKey = referencesNode.ImageKey;
                typesNode.Tag = new TypeNodeBuilder(typesNode, assemblyFile);
                //BuildTypeNode(assemblyFile, typesNode);
                assemblyNode.Nodes.Add(typesNode);

                treeView.Nodes.Add(assemblyNode);
            }
        }

        private void SetTreeNode(AssemblyFile assemblyFile, TreeNode node)
        {
            node.Text = assemblyFile.DisplayName;
            node.Tag = assemblyFile;
            if (assemblyFile.AssemblyDefinition != null)
            {
                switch (assemblyFile.QuickInfo.Architecture)
                {
                    case AssemblyArchitecture.I386:
                        node.ImageKey = "AssemblyFile32";
                        break;
                    case AssemblyArchitecture.AMD64:
                        node.ImageKey = "AssemblyFile64";
                        break;
                    default:
                        node.ImageKey = "AssemblyFile";
                        break;
                }
            }
            else
            {
                node.ImageKey = "AssemblyFileDisabled";
            }
            node.SelectedImageKey = node.ImageKey;

			if (assemblyFile.References.Count == 0 && !assemblyFile.QuickInfo.IsInGac)
			{
				node.ForeColor = System.Drawing.Color.Red;
			}
        }

        private void BuildReferencesNodes(AssemblyFile assemblyFile, TreeNode node)
        {
            foreach (var reference in assemblyFile.References.OrderBy(r => r.FullName))
            {
                var refNode = new TreeNode();
                SetTreeNode(reference, refNode);
                node.Nodes.Add(refNode);
            }
        }

        private void BuildReferencedByNode(AssemblyFile assemblyFile, TreeNode node)
        {
            foreach (var referencedBy in assemblyFile.ReferencedBy)
            {
                var refByNode = new TreeNode();
                SetTreeNode(referencedBy, refByNode);
                node.Nodes.Add(refByNode);
            }
        }



    }
}
