﻿using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DependsTool
{
	public partial class MainForm : Form
	{
		private readonly TextBoxTraceListener textBoxTraceListener;
        private AssemblyFileRepository assemblyFileRepository;

		public class RendererSelection
		{
			public string Name { get; set; }
			public IDependencyGraphRenderer Renderer { get; set; }
		}

		private RendererSelection[] renderers = new[]
		{
			new RendererSelection
			{
				Name = "Yuml",
				Renderer = new YumlGraphGenerator(),
			},
			new RendererSelection
			{
				Name = "D3",
				Renderer = new D3DependencyGraphGenerator(),
			},
			new RendererSelection
			{
				Name = "Dracula",
				Renderer = new DraculaDependencyGraphRenderer(),
			},
		};

		private Properties.Settings FormSettings
        {
            get { return Properties.Settings.Default; }
        }

		public MainForm()
		{
			InitializeComponent();

			textBoxTraceListener = new TextBoxTraceListener(textBoxMessages);
			Trace.Listeners.Add(textBoxTraceListener);
            if (FormSettings.RecentScanPaths == null)
            {
                FormSettings.RecentScanPaths = new System.Collections.Specialized.StringCollection();
            }
            comboBoxFolder.Items.AddRange(FormSettings.RecentScanPaths.Cast<string>().ToArray());
			explorer.DependencyTreeRequested += Explorer_DependencyTreeRequested;
			comboBoxRenderer.DataSource = renderers;
			comboBoxRenderer.DisplayMember = "Name";
			comboBoxRenderer.ValueMember = "Renderer";
		}

		private void buttonScan_Click(object sender, EventArgs e)
		{
			StartScan();
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			CancelScan();
		}

		private async void StartScan()
		{
			if (!Directory.Exists(comboBoxFolder.Text))
			{
				MessageBox.Show(this, "Invalid folder", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			buttonScan.Visible = false;
			progressBar.Visible = true;
            comboBoxFolder.Enabled = false;
            buttonGenerate.Enabled = false;
			tabControl.SelectedTab = tabPageMessages;

			var scanner = new DependencyScanner(comboBoxFolder.Text);
			var scanTask = Task<AssemblyFileRepository>.Factory.StartNew(() =>
				{
					AssemblyFileRepository result = null;
					try
					{
						result = scanner.Scan();
					}
					catch (Exception ex)
					{
						Trace.TraceError("Exception while scanning: " + ex.ToStringStandard());
					}
					return result;
				}
			);
            AddRecentScanPath(comboBoxFolder.Text);
            await scanTask;

			buttonScan.Visible = true;
			progressBar.Visible = false;
            comboBoxFolder.Enabled = true;
            buttonGenerate.Enabled = true;

			explorer.LoadData(scanTask.Result);
			if (scanTask.Result != null)
			{
				tabControl.SelectedTab = tabPageAssemblies;
			}
            this.assemblyFileRepository = scanTask.Result;
		}

        private void AddRecentScanPath(string text)
        {
            const int MaxPathCount = 15;
            var paths = FormSettings.RecentScanPaths;

            // If already exists, remove it
            for (int i = 0; i < paths.Count; )
            {
                if (string.Equals(paths[i], text, StringComparison.CurrentCultureIgnoreCase))
                {
                    paths.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }

            // Ensure we store max 15 paths
            while (paths.Count > MaxPathCount)
            {
                paths.RemoveAt(MaxPathCount);
            }

            // Insert latest at front
            paths.Insert(0, text);
        }

		private void CancelScan()
		{
			throw new NotImplementedException();
		}

		private static string ExceptionToString(Exception ex)
		{
			StringBuilder sb = new StringBuilder();

			int level = 0;

			while (ex != null)
			{
				sb.AppendLine("--Level " + level.ToString() + "--");
				sb.AppendLine("Type: " + ex.GetType().ToString());
				sb.AppendLine("Message: " + ex.Message);
				sb.AppendLine("Source: " + ex.Source);
				sb.AppendLine("StackTrace:" + ex.StackTrace);
				level++;
				ex = ex.InnerException;
			}

			return sb.ToString();
		}

		private void buttonBrowse_Click(object sender, EventArgs e)
		{
			if (comboBoxFolder.Text.Length > 0)
			{
				folderBrowserDialog.SelectedPath = comboBoxFolder.Text;
			}
			if (folderBrowserDialog.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
			{
                comboBoxFolder.Text = folderBrowserDialog.SelectedPath;
			}
		}

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            var settings = new GeneratorSettings
            {
                IncludeDotNetFramework = checkBoxFramework.Checked,
                IncludeVsHost = checkBoxVsHost.Checked,
				IncludeVersion = checkBoxIncludeVersion.Checked,
            };

			var graphGenerator = new GraphGenerator(settings);
			var graph = graphGenerator.GenerateGraph(assemblyFileRepository.Values);
			var rendererSelection = (RendererSelection)comboBoxRenderer.SelectedItem;
			object renderedGraph = rendererSelection.Renderer.Render(graph);
			if (renderedGraph is Image)
			{
				pictureBox1.Image = (Image)renderedGraph;
			}
			else if (renderedGraph is string)
			{
				System.Diagnostics.Process.Start((string)renderedGraph);
			}
        }



		private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            FormSettings.Save();
			Trace.Listeners.Remove(textBoxTraceListener);
        }

		private void Explorer_DependencyTreeRequested(object sender, AssemblyFileEventArgs e)
		{
			ShowDependencyTree(e.AssemblyFile);
		}

		private void ShowDependencyTree(AssemblyFile assemblyFile)
		{
			var view = new DependencyView();
			view.LoadData(assemblyFileRepository, assemblyFile);
			var tab = AddTab(view);
		}

		private TabPage AddTab(DependencyView view)
		{
			var tabPage = new TabPage(view.Title);
			tabPage.Controls.Add(view);
			view.Dock = DockStyle.Fill;
			tabControl.TabPages.Add(tabPage);
			tabControl.SelectedTab = tabPage;
			return tabPage;
		}

	}
}
