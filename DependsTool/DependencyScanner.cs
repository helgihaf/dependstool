﻿using Mono.Cecil;
using System;
using System.Diagnostics;
using System.IO;

namespace DependsTool
{
	public class DependencyScanner
	{
		private string baseDirectory;

		public DependencyScanner(string baseDirectory)
		{
			if (baseDirectory == null)
			{
				throw new ArgumentNullException("baseDirectory");
			}
			this.baseDirectory = baseDirectory;
		}

		public event EventHandler<MessageEventArgs> MessageAvailable;

		public AssemblyFileRepository Scan()
		{
			// readerParameters: Needed by the ReadAssembly function.
			var readerParameters = new ReaderParameters { AssemblyResolver = new CustomAssemblyResolver() };
			var assemblyFiles = new AssemblyFileRepository();

			foreach (var filePath in Directory.EnumerateFiles(baseDirectory))
			{
				if (!IsBinaryFile(filePath))
				{
					continue;
				}

				LogInfo("Loading " + filePath);
				AssemblyDefinition assemblyDefinition = null;
				try
				{
					assemblyDefinition = AssemblyDefinition.ReadAssembly(filePath, readerParameters);
				}
				catch (BadImageFormatException)
				{
				}

				if (assemblyDefinition == null)
				{
					LogWarning(filePath + ": Skipped, bad image format.");
					continue;
				}

				var assemblyFile = new AssemblyFile
				{
					FilePath = filePath,
					AssemblyDefinition = assemblyDefinition,
				};
				try
				{
					assemblyFiles.Add(assemblyFile.FullName, assemblyFile);
				}
				catch (ArgumentException ex)
				{
					Trace.TraceError("Error adding assembly to dictionary: " + ex.Message);
				}
			}

			LogInfo("Processing dependencies...");
			ProcessDependencies(assemblyFiles);

			LogInfo("Done");
			return assemblyFiles;
		}

		private void ProcessDependencies(AssemblyFileRepository assemblyFiles)
		{
			ProcessAssemblyReferences(assemblyFiles);
			ProcessAssemblyReferencedBy(assemblyFiles);
		}

		private void ProcessAssemblyReferences(AssemblyFileRepository assemblyFiles)
		{
			AssemblyFile assemblyFile;
			while ((assemblyFile = GetNextUnprocessedAssemblyFile(assemblyFiles)) != null)
			{
				if (assemblyFile.AssemblyDefinition != null)
				{
					foreach (var reference in assemblyFile.AssemblyDefinition.MainModule.AssemblyReferences)
					{
						var referencedAssemblyFile = GetOrCreateAssemblyFile(assemblyFiles, reference.FullName);
						assemblyFile.References.Add(referencedAssemblyFile);
					}
				}
				assemblyFile.IsProcessed = true;
			}
		}

		private void ProcessAssemblyReferencedBy(AssemblyFileRepository assemblyFiles)
		{
			foreach (var assemblyFile in assemblyFiles.Values)
			{
				foreach (var refAssemblyFile in assemblyFile.References)
				{
					refAssemblyFile.ReferencedBy.Add(assemblyFile);
				}
			}
		}

		private AssemblyFile GetOrCreateAssemblyFile(AssemblyFileRepository assemblyFiles, string assemblyFullName)
		{
			AssemblyFile result;
			if (!assemblyFiles.TryGetValue(assemblyFullName, out result))
			{
				result = new AssemblyFile { FullName = assemblyFullName };
				assemblyFiles.Add(result.FullName, result);
			}
			return result;
		}

		private AssemblyFile GetNextUnprocessedAssemblyFile(AssemblyFileRepository assemblyFiles)
		{
			foreach (var assemblyFile in assemblyFiles.Values)
			{
				if (!assemblyFile.IsProcessed)
				{
					return assemblyFile;
				}
			}

			return null;
		}

		private void LogInfo(string message)
		{
			Log(message);
		}

		private void LogWarning(string message)
		{
			Log("Warning: " + message);
		}

		private void Log(string message)
		{
			Trace.TraceInformation(message);
			if (MessageAvailable != null)
			{
				MessageAvailable(this, new MessageEventArgs { Message = message });
			}
		}

		private bool IsBinaryFile(string filePath)
		{
			var extension = Path.GetExtension(filePath).ToLowerInvariant();
			return extension == ".dll" || extension == ".exe";
		}
	}

	public class MessageEventArgs : EventArgs
	{
		public string Message { get; set; }
	}
}
