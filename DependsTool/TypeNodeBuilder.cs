﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Mono.Cecil;

namespace DependsTool
{
    class TypeNodeBuilder : INodeBuilder
    {
        private readonly TreeNode parent;
        private readonly AssemblyFile assemblyFile;

        public TypeNodeBuilder(TreeNode parent, AssemblyFile assemblyFile)
        {
            this.parent = parent;
            this.assemblyFile = assemblyFile;

            // Dummy node
            parent.Nodes.Add(new TreeNode());
        }

        public void Build(AssemblyFileFilter filter)
        {
            if (!(parent.Nodes.Count == 1 && parent.Nodes[0].Tag == null))
                return;

            parent.Nodes.Clear();
            var namespaces = OrganizeTypes(assemblyFile);
            foreach (var namespaceKeyValue in namespaces.OrderBy(n => n.Key))
            {
                var namespaceNode = new TreeNode();
                SetTreeNodeForNamespace(namespaceNode, namespaceKeyValue.Key);
                foreach (var type in namespaceKeyValue.Value.OrderBy(t => t.Name))
                {
                    var typeNode = new TreeNode();
                    SetTreeNodeForType(typeNode, type);
                    namespaceNode.Nodes.Add(typeNode);
                }
                parent.Nodes.Add(namespaceNode);
            }
        }

        private void SetTreeNodeForNamespace(TreeNode node, string nameSpace)
        {
            node.Text = nameSpace;
            node.ImageKey = "Namespace";
            node.SelectedImageKey = node.ImageKey;
            node.Tag = nameSpace;
        }

        private void SetTreeNodeForType(TreeNode typeNode, TypeDefinition type)
        {
            typeNode.Text = type.Name;
            typeNode.ImageKey = "Type";
            typeNode.SelectedImageKey = typeNode.ImageKey;
            typeNode.Tag = type;
        }

        private static IDictionary<string, List<Mono.Cecil.TypeDefinition>> OrganizeTypes(AssemblyFile assemblyFile)
        {
            var namespaces = new Dictionary<string, List<Mono.Cecil.TypeDefinition>>();
            foreach (var type in assemblyFile.AssemblyDefinition.Modules.SelectMany(m => m.Types))
            {
                List<Mono.Cecil.TypeDefinition> typeList;
                if (!namespaces.TryGetValue(type.Namespace, out typeList))
                {
                    typeList = new List<Mono.Cecil.TypeDefinition>();
                    namespaces.Add(type.Namespace, typeList);
                }
                typeList.Add(type);
            }

            return namespaces;
        }
    }
}

