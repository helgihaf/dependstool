﻿using System;
using System.Collections.Generic;

namespace DependsTool
{
    [Serializable]
    public class AssemblyFileRepository : Dictionary<string, AssemblyFile>
    {
        public AssemblyFileRepository()
            : base(StringComparer.OrdinalIgnoreCase)
        {
        }

        protected AssemblyFileRepository(System.Runtime.Serialization.SerializationInfo serializationInfo, System.Runtime.Serialization.StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }
    }
}
