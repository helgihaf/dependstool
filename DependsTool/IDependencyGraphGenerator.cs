﻿using QuickGraph;

namespace DependsTool
{
	public interface IDependencyGraphRenderer
	{
		object Render(IVertexAndEdgeListGraph<string, SEquatableEdge<string>> graph);
	}
}
