﻿using System.Collections.Generic;

namespace DependsTool
{
    internal class AssemblyFileFilterFactory
    {
        public static AssemblyFileFilter Create(SearchType searchType, string text, bool referenced, bool unreferenced)
        {
            if (referenced == unreferenced == true && string.IsNullOrEmpty(text))
                return null;

            var list = new List<AssemblyFileFilter>();

            if ((searchType & SearchType.Assembly) != 0)
                list.Add(new AssemblyFileFilterForFullName() { Referenced = referenced, Unreferenced = unreferenced, Text = text });

            if ((searchType & SearchType.DefinedType) != 0)
                list.Add(new AssemblyFileFilterForDefinedType() { Referenced = referenced, Unreferenced = unreferenced, Text = text });

            if ((searchType & SearchType.ReferencedType) != 0)
                list.Add(new AssemblyFileFilterForReferencedType() { Referenced = referenced, Unreferenced = unreferenced, Text = text });

            if (list.Count == 0)
                return null;

            if (list.Count == 1)
                return list[0];

            return new AssemblyFileFilterCollection(list);
        }
    }
}