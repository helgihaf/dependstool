﻿using System;
using System.IO;
using System.Text;
using QuickGraph;

namespace DependsTool
{
	internal class DraculaDependencyGraphRenderer : IDependencyGraphRenderer
	{
		public object Render(IVertexAndEdgeListGraph<string, SEquatableEdge<string>> graph)
		{
			string js = GenerateDraculaJs(graph);
			return CreateGraphFiles(js);
		}

		private string CreateGraphFiles(string js)
		{
			string fullJs = File.ReadAllText("Dracula\\graphTemplate.js");
			File.WriteAllText("Dracula\\graph.js", fullJs.Replace("/*{89362D36-6E07-42A7-B677-BBF1E734C176}*/", js));
			return Path.Combine(Environment.CurrentDirectory, "Dracula\\graph.html");
		}

		private string GenerateDraculaJs(IVertexAndEdgeListGraph<string, SEquatableEdge<string>> graph)
		{
			const string directed = "{ directed : true }";
			var sb = new StringBuilder();
			foreach (var node in graph.Vertices)
			{
				sb.AppendLine(string.Format("    g.addNode(\"{0}\");", node));
			}
			foreach (var edge in graph.Edges)
			{
				sb.AppendLine(string.Format("    g.addEdge(\"{0}\", \"{1}\", {2});", edge.Source, edge.Target, directed));
			}
			return sb.ToString();
		}
	}
}