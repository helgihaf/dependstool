﻿using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace DependsTool
{
	public partial class DependencyTreeView : UserControl
	{
		public DependencyTreeView()
		{
			InitializeComponent();
		}

		public void LoadData(AssemblyFileRepository repository, AssemblyFile assemblyFile)
		{
			treeView.Nodes.Clear();
			treeView.BeginUpdate();
			AddNode(treeView.Nodes, assemblyFile);
			treeView.EndUpdate();
		}

		private void AddNode(TreeNodeCollection nodes, AssemblyFile assemblyFile)
		{
			var node = new TreeNode();
			node.Text = assemblyFile.FullName;
			node.Tag = assemblyFile;
			nodes.Add(node);
			foreach (var referencedFile in assemblyFile.References.OrderBy(a => a.FullName))
			{
				AddNode(node.Nodes, referencedFile);
			}
		}
	}
}
