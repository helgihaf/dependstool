﻿using System;
using System.Reflection;

namespace DependsTool
{

	[Serializable]
	internal sealed class DomainWork
	{
		// In
		public string AssemblyFilePath { get; set; }

		// Out
		public readonly DomainWorkResult Result = new DomainWorkResult();

		public void Do()
		{
			Assembly assembly = Assembly.LoadFrom(AssemblyFilePath);
			var types = assembly.GetTypes();
			Result.TypeCount = types.Length;
			Result.MemberCount = 0;
			foreach (Type type in types)
			{
				Result.MemberCount += type.GetMembers().Length;
			}
		}
	}

	internal sealed class DomainWorkResult : MarshalByRefObject
	{
		public long TypeCount { get; set; }
		public long MemberCount { get; set; }

	}

}