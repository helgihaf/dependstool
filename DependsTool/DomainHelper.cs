﻿using System;
using System.IO;

namespace DependsTool
{
	internal static class DomainHelper
	{
		public static void Execute(CrossAppDomainDelegate callBack)
		{
			string applicationBase = Path.GetDirectoryName(callBack.Target.GetType().Assembly.Location);
			var setup = new AppDomainSetup
			{
				ShadowCopyFiles = "true",
				ApplicationBase = applicationBase
			};

			
			AppDomain dom = AppDomain.CreateDomain("WorkDomin", null, setup);
			try
			{
				dom.DoCallBack(callBack);
			}
			finally
			{
				AppDomain.Unload(dom);
			}
		}
	}

}
