﻿using System.Linq;

namespace DependsTool
{
	class AssemblyFileFilterForDefinedType : AssemblyFileFilter
	{
		public override bool Include(AssemblyFile assemblyFile)
		{
			if (!base.Include(assemblyFile))
			{
				return false;
			}

			if (assemblyFile.AssemblyDefinition == null)
			{
				return false;
			}

			foreach (var module in assemblyFile.AssemblyDefinition.Modules)
			{
				if (module.Types.Any(t => IncludeString(t.FullName)))
				{
					return true;
				}
			}
			return false;
		}
	}
}
