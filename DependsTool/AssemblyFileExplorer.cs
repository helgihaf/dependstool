﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;

namespace DependsTool
{
    public partial class AssemblyFileExplorer : UserControl
    {
        private AssemblyFileRepository repository;
        private TreeNode lastHoverNode;
        private int lastHoverCount;

        public AssemblyFileExplorer()
        {
            InitializeComponent();
            comboBoxType.SelectedIndex = 1;
        }

        public void LoadData(AssemblyFileRepository repository)
        {
            this.repository = repository;
            BuildTree();
        }

        public event EventHandler<AssemblyFileEventArgs> DependencyTreeRequested;

        private void BuildTree()
        {
            treeView.BeginUpdate();
            treeView.Nodes.Clear();

            var filter = GetFilter();

            var builder = new AssemblyFileNodeBuilder(treeView, repository);
            builder.Build(filter);
            treeView.EndUpdate();
        }

        private AssemblyFileFilter GetFilter()
        {
            var searchType = GetSelectedSearchType();
            var text = textBoxFilter.Text;
            return AssemblyFileFilterFactory.Create(searchType, text, checkBoxReferenced.Checked, checkBoxUnreferenced.Checked);
        }

        private void BuildReferencesNodes(AssemblyFile assemblyFile, TreeNode node)
        {
            foreach (var reference in assemblyFile.References.OrderBy(r => r.FullName))
            {
                var refNode = new TreeNode();
                SetTreeNode(reference, refNode);
                node.Nodes.Add(refNode);
            }
        }

        private void BuildReferencedByNode(AssemblyFile assemblyFile, TreeNode node)
        {
            foreach (var referencedBy in assemblyFile.ReferencedBy)
            {
                var refByNode = new TreeNode();
                SetTreeNode(referencedBy, refByNode);
                node.Nodes.Add(refByNode);
            }
        }

        private void SetTreeNode(AssemblyFile assemblyFile, TreeNode node)
        {
            node.Text = assemblyFile.DisplayName;
            node.Tag = assemblyFile;
            if (assemblyFile.AssemblyDefinition != null)
            {
                switch (assemblyFile.QuickInfo.Architecture)
                {
                    case AssemblyArchitecture.I386:
                        node.ImageKey = "AssemblyFile32";
                        break;
                    case AssemblyArchitecture.AMD64:
                        node.ImageKey = "AssemblyFile64";
                        break;
                    default:
                        node.ImageKey = "AssemblyFile";
                        break;
                }
            }
            else
            {
                node.ImageKey = "AssemblyFileDisabled";
            }
            node.SelectedImageKey = node.ImageKey;
        }

        private void textBoxFilter_TextChanged(object sender, EventArgs e)
        {
            InvalidateFilter();
        }

        private void InvalidateFilter()
        {
            if (this.repository != null)
            {
                BuildTree();
            }
        }

        private void treeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Level == 2)
            {
                textBoxFilter.Text = e.Node.Text;
            }
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(treeView.SelectedNode.Text);
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var assemblyFile = e.Node.Tag as AssemblyFile;
            if (assemblyFile != null)
            {
                propertyGrid.SelectedObject = assemblyFile.QuickInfo;
            }
        }


        private SearchType GetSelectedSearchType()
        {
            SearchType result = SearchType.Any;
            switch (comboBoxType.SelectedIndex)
            {
                case 1:
                    result = SearchType.Assembly;
                    break;
                case 2:
                    result = SearchType.DefinedType;
                    break;
                case 3:
                    result = SearchType.ReferencedType;
                    break;
            }
            return result;
        }

        private void comboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            InvalidateFilter();
        }

        private void treeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            var nodeBuilder = e.Node.Tag as INodeBuilder;
            if (nodeBuilder != null)
            {
                nodeBuilder.Build(null);
            }
        }

        private void tryLoadFromToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TryLoadFrom((AssemblyFile)treeView.SelectedNode.Tag);
        }

        private void TryLoadFrom(AssemblyFile tag)
        {
            string message = null;
            MessageBoxIcon icon = MessageBoxIcon.Information;

            var work = new DomainWork { AssemblyFilePath = tag.FilePath };
            try
            {
                DomainHelper.Execute(work.Do);
            }
            catch (Exception ex)
            {
                message = ex.Message + "\n\nSee Messages tab or Trace log for further information.";
                Trace.TraceError(ex.ToStringStandard());
                icon = MessageBoxIcon.Error;
            }

            if (message == null)
            {
                message = "Success, assembly contains " + work.Result.MemberCount + " members in " + work.Result.TypeCount + " types.";
            }

            MessageBox.Show(this, message, "Try Assembly.LoadFrom", MessageBoxButtons.OK, icon);
        }

        private void showDependencyTreeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OnDependencyTreeRequested((AssemblyFile)treeView.SelectedNode.Tag);
        }

        private void OnDependencyTreeRequested(AssemblyFile assemblyFile)
        {
            if (DependencyTreeRequested != null)
            {
                DependencyTreeRequested(this, new AssemblyFileEventArgs { AssemblyFile = assemblyFile });
            }
        }

        private void checkBoxReferenced_CheckedChanged(object sender, EventArgs e)
        {
            InvalidateFilter();

        }

        private void checkBoxUnreferenced_CheckedChanged(object sender, EventArgs e)
        {
            InvalidateFilter();
        }

        private void treeView_NodeMouseHover(object sender, TreeNodeMouseHoverEventArgs e)
        {
            SetToolTipOfNode(e.Node);
        }

        private void SetToolTipOfNode(TreeNode theNode)
        {
            //TreeNode theNode = this.treeView1.GetNodeAt(e.X, e.Y);

            int nodeCount = 0;
            if (theNode != null)
            {
                nodeCount = theNode.Nodes.Count;
                if (!object.ReferenceEquals(lastHoverNode, theNode) || nodeCount != lastHoverCount)
                {
                    string childNodes = nodeCount.ToString() + " child nodes";
                    this.toolTip.SetToolTip(this.treeView, childNodes);
                }
            }
            else
            {
                this.toolTip.SetToolTip(this.treeView, "");
            }

            lastHoverNode = theNode;
            lastHoverCount = nodeCount;
        }
    }
}
