﻿namespace DependsTool
{
    partial class AssemblyFileExplorer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AssemblyFileExplorer));
			this.treeView = new System.Windows.Forms.TreeView();
			this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tryLoadFromToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.showDependencyTreeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.imageList = new System.Windows.Forms.ImageList(this.components);
			this.panelTop = new System.Windows.Forms.Panel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.checkBoxReferenced = new System.Windows.Forms.CheckBox();
			this.checkBoxUnreferenced = new System.Windows.Forms.CheckBox();
			this.comboBoxType = new System.Windows.Forms.ComboBox();
			this.textBoxFilter = new System.Windows.Forms.TextBox();
			this.splitContainer = new System.Windows.Forms.SplitContainer();
			this.propertyGrid = new System.Windows.Forms.PropertyGrid();
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			this.contextMenuStrip.SuspendLayout();
			this.panelTop.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
			this.splitContainer.Panel1.SuspendLayout();
			this.splitContainer.Panel2.SuspendLayout();
			this.splitContainer.SuspendLayout();
			this.SuspendLayout();
			// 
			// treeView
			// 
			this.treeView.ContextMenuStrip = this.contextMenuStrip;
			this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView.ImageIndex = 0;
			this.treeView.ImageList = this.imageList;
			this.treeView.Location = new System.Drawing.Point(0, 0);
			this.treeView.Name = "treeView";
			this.treeView.SelectedImageIndex = 0;
			this.treeView.Size = new System.Drawing.Size(377, 300);
			this.treeView.TabIndex = 0;
			this.treeView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView_BeforeExpand);
			this.treeView.NodeMouseHover += new System.Windows.Forms.TreeNodeMouseHoverEventHandler(this.treeView_NodeMouseHover);
			this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
			this.treeView.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView_NodeMouseDoubleClick);
			// 
			// contextMenuStrip
			// 
			this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem,
            this.tryLoadFromToolStripMenuItem,
            this.showDependencyTreeToolStripMenuItem});
			this.contextMenuStrip.Name = "contextMenuStrip";
			this.contextMenuStrip.Size = new System.Drawing.Size(192, 70);
			// 
			// copyToolStripMenuItem
			// 
			this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
			this.copyToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
			this.copyToolStripMenuItem.Text = "&Copy full name";
			this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
			// 
			// tryLoadFromToolStripMenuItem
			// 
			this.tryLoadFromToolStripMenuItem.Name = "tryLoadFromToolStripMenuItem";
			this.tryLoadFromToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
			this.tryLoadFromToolStripMenuItem.Text = "Load &assembly";
			this.tryLoadFromToolStripMenuItem.Click += new System.EventHandler(this.tryLoadFromToolStripMenuItem_Click);
			// 
			// showDependencyTreeToolStripMenuItem
			// 
			this.showDependencyTreeToolStripMenuItem.Name = "showDependencyTreeToolStripMenuItem";
			this.showDependencyTreeToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
			this.showDependencyTreeToolStripMenuItem.Text = "Show full &dependency";
			this.showDependencyTreeToolStripMenuItem.Click += new System.EventHandler(this.showDependencyTreeToolStripMenuItem_Click);
			// 
			// imageList
			// 
			this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
			this.imageList.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList.Images.SetKeyName(0, "Folder");
			this.imageList.Images.SetKeyName(1, "AssemblyFileDisabled");
			this.imageList.Images.SetKeyName(2, "AssemblyFile");
			this.imageList.Images.SetKeyName(3, "AssemblyFile32");
			this.imageList.Images.SetKeyName(4, "AssemblyFile64");
			this.imageList.Images.SetKeyName(5, "Namespace");
			this.imageList.Images.SetKeyName(6, "Type");
			// 
			// panelTop
			// 
			this.panelTop.Controls.Add(this.groupBox1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(546, 74);
			this.panelTop.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.checkBoxReferenced);
			this.groupBox1.Controls.Add(this.checkBoxUnreferenced);
			this.groupBox1.Controls.Add(this.comboBoxType);
			this.groupBox1.Controls.Add(this.textBoxFilter);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(546, 74);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Filter";
			// 
			// checkBoxReferenced
			// 
			this.checkBoxReferenced.AutoSize = true;
			this.checkBoxReferenced.Checked = true;
			this.checkBoxReferenced.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxReferenced.Location = new System.Drawing.Point(6, 45);
			this.checkBoxReferenced.Name = "checkBoxReferenced";
			this.checkBoxReferenced.Size = new System.Drawing.Size(82, 17);
			this.checkBoxReferenced.TabIndex = 3;
			this.checkBoxReferenced.Text = "Referenced";
			this.checkBoxReferenced.UseVisualStyleBackColor = true;
			this.checkBoxReferenced.CheckedChanged += new System.EventHandler(this.checkBoxReferenced_CheckedChanged);
			// 
			// checkBoxUnreferenced
			// 
			this.checkBoxUnreferenced.AutoSize = true;
			this.checkBoxUnreferenced.Checked = true;
			this.checkBoxUnreferenced.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxUnreferenced.Location = new System.Drawing.Point(94, 45);
			this.checkBoxUnreferenced.Name = "checkBoxUnreferenced";
			this.checkBoxUnreferenced.Size = new System.Drawing.Size(91, 17);
			this.checkBoxUnreferenced.TabIndex = 2;
			this.checkBoxUnreferenced.Text = "Unreferenced";
			this.checkBoxUnreferenced.UseVisualStyleBackColor = true;
			this.checkBoxUnreferenced.CheckedChanged += new System.EventHandler(this.checkBoxUnreferenced_CheckedChanged);
			// 
			// comboBoxType
			// 
			this.comboBoxType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxType.FormattingEnabled = true;
			this.comboBoxType.Items.AddRange(new object[] {
            "Any",
            "Assembly",
            "Defined type",
            "Referenced type"});
			this.comboBoxType.Location = new System.Drawing.Point(387, 19);
			this.comboBoxType.Name = "comboBoxType";
			this.comboBoxType.Size = new System.Drawing.Size(153, 21);
			this.comboBoxType.TabIndex = 1;
			this.comboBoxType.SelectedIndexChanged += new System.EventHandler(this.comboBoxType_SelectedIndexChanged);
			// 
			// textBoxFilter
			// 
			this.textBoxFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxFilter.Location = new System.Drawing.Point(6, 19);
			this.textBoxFilter.Name = "textBoxFilter";
			this.textBoxFilter.Size = new System.Drawing.Size(375, 20);
			this.textBoxFilter.TabIndex = 0;
			this.textBoxFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
			// 
			// splitContainer
			// 
			this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer.Location = new System.Drawing.Point(0, 74);
			this.splitContainer.Name = "splitContainer";
			// 
			// splitContainer.Panel1
			// 
			this.splitContainer.Panel1.Controls.Add(this.treeView);
			// 
			// splitContainer.Panel2
			// 
			this.splitContainer.Panel2.Controls.Add(this.propertyGrid);
			this.splitContainer.Size = new System.Drawing.Size(546, 300);
			this.splitContainer.SplitterDistance = 377;
			this.splitContainer.TabIndex = 2;
			// 
			// propertyGrid
			// 
			this.propertyGrid.CategoryForeColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.propertyGrid.Location = new System.Drawing.Point(0, 0);
			this.propertyGrid.Name = "propertyGrid";
			this.propertyGrid.Size = new System.Drawing.Size(165, 300);
			this.propertyGrid.TabIndex = 0;
			// 
			// AssemblyFileExplorer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.splitContainer);
			this.Controls.Add(this.panelTop);
			this.Name = "AssemblyFileExplorer";
			this.Size = new System.Drawing.Size(546, 374);
			this.contextMenuStrip.ResumeLayout(false);
			this.panelTop.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.splitContainer.Panel1.ResumeLayout(false);
			this.splitContainer.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
			this.splitContainer.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.ImageList imageList;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
		private System.Windows.Forms.SplitContainer splitContainer;
		private System.Windows.Forms.PropertyGrid propertyGrid;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.TextBox textBoxFilter;
		private System.Windows.Forms.ToolStripMenuItem tryLoadFromToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem showDependencyTreeToolStripMenuItem;
		private System.Windows.Forms.CheckBox checkBoxUnreferenced;
		private System.Windows.Forms.ToolTip toolTip;
		private System.Windows.Forms.CheckBox checkBoxReferenced;
	}
}
