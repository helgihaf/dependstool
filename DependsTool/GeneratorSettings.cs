﻿using Mono.Cecil;
using System.Collections.Generic;
using System.Linq;

namespace DependsTool
{
	public class GeneratorSettings
	{
		public bool IncludeDotNetFramework { get; set; }
		public bool IncludeVsHost { get; set; }
		public bool IncludeVersion { get; set; }


		public bool ShouldIncludeFile(AssemblyFile assemblyFile)
		{
			if (!IncludeDotNetFramework && assemblyFile.IsDotNetFramework)
			{
				return false;
			}

			if (!IncludeVsHost && assemblyFile.IsVsHost)
			{
				return false;
			}

			return true;
		}

		public IEnumerable<AssemblyFile> Select(IEnumerable<AssemblyFile> assemblyFiles)
		{
			return assemblyFiles.Where(a => ShouldIncludeFile(a));
		}

		public string GetName(AssemblyFile assemblyFile)
		{
			AssemblyNameReference assemblyNameReference;
			if (assemblyFile.AssemblyDefinition != null)
			{
				assemblyNameReference = assemblyFile.AssemblyDefinition.Name;
			}
			else
			{
				assemblyNameReference = AssemblyNameReference.Parse(assemblyFile.FullName);
			}

			if (!IncludeVersion)
			{
				return assemblyNameReference.Name;
			}
			else
			{
				return assemblyNameReference.Name + " " + assemblyNameReference.Version.ToString();
			}
		}
	}
}
