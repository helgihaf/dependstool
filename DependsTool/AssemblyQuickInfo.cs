﻿using Mono.Cecil;
using System;
using System.Text;

namespace DependsTool
{
	public class AssemblyQuickInfo
	{
		private AssemblyFile assemblyFile;
		private bool? isInGac;

		public AssemblyQuickInfo(AssemblyFile assemblyFile)
		{
			this.assemblyFile = assemblyFile;
		}

		public string FilePath
		{
			get { return assemblyFile.FilePath; }
		}

		public string Name
		{
			get
			{
				if (assemblyFile.AssemblyDefinition != null)
				{
					return assemblyFile.AssemblyDefinition.Name.Name;
				}
				else
				{
					return null;
				}
			}
		}

		public Version Version
		{
			get
			{
				if (assemblyFile.AssemblyDefinition != null)
				{
					return assemblyFile.AssemblyDefinition.Name.Version;
				}
				else
				{
					return null;
				}
			}
		}

		public string Culture
		{
			get
			{
				if (assemblyFile.AssemblyDefinition != null)
				{
					return assemblyFile.AssemblyDefinition.Name.Culture;
				}
				else
				{
					return null;
				}
			}
		}

		public string PublicKeyToken
		{
			get
			{
				if (assemblyFile.AssemblyDefinition != null)
				{
					return BytesToString(assemblyFile.AssemblyDefinition.Name.PublicKeyToken);
				}
				else
				{
					return null;
				}
			}
		}

		public AssemblyArchitecture Architecture
		{
			get 
			{
				if (assemblyFile.AssemblyDefinition != null)
				{
					return GetArchitecture(assemblyFile.AssemblyDefinition);
				}
				else
				{
					return AssemblyArchitecture.Unknown;
				}
			}
		}


		public TargetRuntime? Runtime
		{
			get
			{
				if (assemblyFile.AssemblyDefinition != null)
				{
					return assemblyFile.AssemblyDefinition.MainModule.Runtime;
				}
				else
				{
					return null;
				}
			}
		}


		public bool IsInGac
		{
			get
			{
				if (!isInGac.HasValue)
				{
					isInGac =
						GacUtil.IsAssemblyInGAC(assemblyFile.FullName + ", processorArchitecture=MSIL")
						|| GacUtil.IsAssemblyInGAC(assemblyFile.FullName + ", processorArchitecture=AMD64")
						|| GacUtil.IsAssemblyInGAC(assemblyFile.FullName + ", processorArchitecture=x86")
						;
				}
				return isInGac.Value;
			}
		}



		private static AssemblyArchitecture GetArchitecture(Mono.Cecil.AssemblyDefinition assemblyDefinition)
		{
			AssemblyArchitecture result;

            switch (assemblyDefinition.MainModule.Architecture)
            {
                case Mono.Cecil.TargetArchitecture.AMD64:
                    result = AssemblyArchitecture.AMD64;
                    break;
                case Mono.Cecil.TargetArchitecture.I386:
                    if ((assemblyDefinition.MainModule.Attributes & Mono.Cecil.ModuleAttributes.ILOnly) != 0)
                    {
                        result = AssemblyArchitecture.AnyCPU;
                    }
                    else
                    {
                        result = AssemblyArchitecture.I386;
                    }
                    break;
                case Mono.Cecil.TargetArchitecture.IA64:
                    result = AssemblyArchitecture.IA64;
                    break;
                default:
                    result = AssemblyArchitecture.Unknown;
                    break;
            }

			return result;
		}

		private static string BytesToString(byte[] bytes)
		{
			var sb = new StringBuilder();
			for (int i = 0; i < bytes.Length; i++)
			{
				sb.AppendFormat("{0:X2}", bytes[i]);
			}
			return sb.ToString();
		}
	}
}
