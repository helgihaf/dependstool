﻿using System.Text;
using QuickGraph;

namespace DependsTool
{
	internal class YumlGraphGenerator : IDependencyGraphRenderer
	{
		public object Render(IVertexAndEdgeListGraph<string, SEquatableEdge<string>> graph)
		{
			string yuml = GenerateYuml(graph);
			return GetGraphUrl(yuml);
		}

		private string GetGraphUrl(string yuml)
		{
			return "http://yuml.me/diagram/scruffy/class/edit/" + yuml;
		}

		private string GenerateYuml(IVertexAndEdgeListGraph<string, SEquatableEdge<string>> graph)
		{
			var sb = new StringBuilder();
			foreach (var edge in graph.Edges)
			{
				if (sb.Length > 0)
				{
					sb.Append(",");
				}
				sb.Append("[" + edge.Source + "]->[" + edge.Target + "]");
			}
			return sb.ToString();
		}

	}
}
