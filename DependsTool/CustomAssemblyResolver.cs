﻿using Mono.Cecil;

namespace DependsTool
{
    internal sealed class CustomAssemblyResolver : IAssemblyResolver
    {
        public void Dispose()
        {
            // Nothing
        }

        public AssemblyDefinition Resolve(AssemblyNameReference name)
        {
            return null;
        }

        public AssemblyDefinition Resolve(AssemblyNameReference name, ReaderParameters parameters)
        {
            return null;
        }

        public AssemblyDefinition Resolve(string fullName)
        {
            return null;
        }

        public AssemblyDefinition Resolve(string fullName, ReaderParameters parameters)
        {
            return null;
        }
    }

}
