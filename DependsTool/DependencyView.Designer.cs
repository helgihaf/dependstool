﻿namespace DependsTool
{
	partial class DependencyView
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DependencyView));
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.buttonView = new System.Windows.Forms.ToolStripDropDownButton();
			this.menuItemList = new System.Windows.Forms.ToolStripMenuItem();
			this.menuItemTree = new System.Windows.Forms.ToolStripMenuItem();
			this.treeView = new DependsTool.DependencyTreeView();
			this.listView = new DependsTool.DependencyListView();
			this.toolStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// toolStrip1
			// 
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonView});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(501, 25);
			this.toolStrip1.TabIndex = 0;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// buttonView
			// 
			this.buttonView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.buttonView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemList,
            this.menuItemTree});
			this.buttonView.Image = ((System.Drawing.Image)(resources.GetObject("buttonView.Image")));
			this.buttonView.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.buttonView.Name = "buttonView";
			this.buttonView.Size = new System.Drawing.Size(164, 22);
			this.buttonView.Text = "toolStripDropDownButton1";
			// 
			// menuItemList
			// 
			this.menuItemList.Name = "menuItemList";
			this.menuItemList.Size = new System.Drawing.Size(152, 22);
			this.menuItemList.Text = "List";
			this.menuItemList.Click += new System.EventHandler(this.menuItemList_Click);
			// 
			// menuItemTree
			// 
			this.menuItemTree.Name = "menuItemTree";
			this.menuItemTree.Size = new System.Drawing.Size(152, 22);
			this.menuItemTree.Text = "Tree";
			this.menuItemTree.Click += new System.EventHandler(this.menuItemTree_Click);
			// 
			// treeView
			// 
			this.treeView.Location = new System.Drawing.Point(165, 76);
			this.treeView.Name = "treeView";
			this.treeView.Size = new System.Drawing.Size(451, 422);
			this.treeView.TabIndex = 2;
			this.treeView.Visible = false;
			// 
			// listView
			// 
			this.listView.Location = new System.Drawing.Point(40, 99);
			this.listView.Name = "listView";
			this.listView.Size = new System.Drawing.Size(474, 347);
			this.listView.TabIndex = 1;
			// 
			// DependencyView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.treeView);
			this.Controls.Add(this.listView);
			this.Controls.Add(this.toolStrip1);
			this.Name = "DependencyView";
			this.Size = new System.Drawing.Size(501, 404);
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip toolStrip1;
		private DependencyListView listView;
		private DependencyTreeView treeView;
		private System.Windows.Forms.ToolStripDropDownButton buttonView;
		private System.Windows.Forms.ToolStripMenuItem menuItemList;
		private System.Windows.Forms.ToolStripMenuItem menuItemTree;
	}
}
