﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace DependsTool
{
	public partial class DependencyListView : UserControl
	{
		private AssemblyFile focusFile;

		public DependencyListView()
		{
			InitializeComponent();
		}

		private class FilePair
		{
			public AssemblyFile To;
			public AssemblyFile From;
		}

		private class ViewModelItem
		{
			public AssemblyFile AssemblyFile;
			public int FromCount;
			public string From;
		}

		public void LoadData(AssemblyFileRepository repository, AssemblyFile assemblyFile)
		{
			focusFile = assemblyFile;

			listView.Items.Clear();
			listView.BeginUpdate();
			foreach (var viewModelItem in GetAllReferencedFilesRecursive(assemblyFile).OrderBy(vmi => vmi.AssemblyFile.FullName))
			{
				var item = new ListViewItem();
				item.Text = viewModelItem.AssemblyFile.FullName;
				item.SubItems.Add(viewModelItem.FromCount.ToString());
				if (viewModelItem.From != null)
				{
					item.SubItems.Add(viewModelItem.From);
				}
				listView.Items.Add(item);
			}
			listView.EndUpdate();
		}

		private IEnumerable<ViewModelItem> GetAllReferencedFilesRecursive(AssemblyFile assemblyFile)
		{
			var filePairs = new List<FilePair>();
			CollectReferences(filePairs, assemblyFile);

			return
				from filePair in filePairs
				group filePair by filePair.To into g
				select new ViewModelItem { AssemblyFile = g.Key, FromCount = g.Count(), From = FormatFileList(g) };
		}

		private string FormatFileList(IEnumerable<FilePair> filePairs)
		{
			return filePairs.Select(fp => fp.From.ShortName).Aggregate((current, next) => current + ", " + next);
		}

		private void CollectReferences(List<FilePair> fileTuples, AssemblyFile assemblyFile)
		{
			foreach (var referencedFile in assemblyFile.References)
			{
				fileTuples.Add(new FilePair { To = referencedFile, From = assemblyFile });
				CollectReferences(fileTuples, referencedFile);
			}

		}

		private void AddNode(TreeNodeCollection nodes, AssemblyFile assemblyFile)
		{
			var node = new TreeNode();
			node.Text = assemblyFile.FullName;
			node.Tag = assemblyFile;
			nodes.Add(node);
			foreach (var referencedFile in assemblyFile.References)
			{
				AddNode(node.Nodes, referencedFile);
			}
		}

	}
}
