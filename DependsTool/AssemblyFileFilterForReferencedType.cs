﻿namespace DependsTool
{
	class AssemblyFileFilterForReferencedType : AssemblyFileFilter
	{
		public override bool Include(AssemblyFile assemblyFile)
		{
			if (!base.Include(assemblyFile))
			{
				return false;
			}

			if (assemblyFile.AssemblyDefinition == null)
				return false;

			foreach (var module in assemblyFile.AssemblyDefinition.Modules)
			{
				foreach (var typeReference in module.GetTypeReferences())
				{
					if (IncludeString(typeReference.FullName))
						return true;
				}
			}
			return false;
		}
	}
}
