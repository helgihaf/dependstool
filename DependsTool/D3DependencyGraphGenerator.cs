﻿using System;
using System.IO;
using System.Text;
using QuickGraph;

namespace DependsTool
{
	internal class D3DependencyGraphGenerator : IDependencyGraphRenderer
	{
		public object Render(IVertexAndEdgeListGraph<string, SEquatableEdge<string>> graph)
		{
			string js = GenerateD3Js(graph);
			return CreateGraphFiles(js);
		}

		private string CreateGraphFiles(string js)
		{
			string fullJs = File.ReadAllText("D3\\graphTemplate.html");
			File.WriteAllText("D3\\graph.html", fullJs.Replace("/*{89362D36-6E07-42A7-B677-BBF1E734C176}*/", js));
			return Path.Combine(Environment.CurrentDirectory, "D3\\graph.html");
		}

		private string GenerateD3Js(IVertexAndEdgeListGraph<string, SEquatableEdge<string>> graph)
		{
			var sb = new StringBuilder();
			foreach (var edge in graph.Edges)
			{
				//{ source: "Microsoft", target: "Amazon", type: "licensing" },
				if (sb.Length > 0)
				{
					sb.AppendLine(",");
				}
				sb.Append(
					"{ source: \"" + edge.Source + "\", target: \"" + edge.Target + "\", type: \"licensing\" }");
			}
			return sb.ToString();
		}
 
	}
}
