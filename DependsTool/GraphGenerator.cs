﻿using QuickGraph;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DependsTool
{
	public class GraphGenerator
	{
		private readonly GeneratorSettings settings;

		public GraphGenerator(GeneratorSettings settings)
		{
			if (settings == null)
			{
				throw new ArgumentNullException(nameof(settings));
			}
			this.settings = settings;
		}

		public IVertexAndEdgeListGraph<string, SEquatableEdge<string>> GenerateGraph(IEnumerable<AssemblyFile> assemblyFiles)
		{
			var dictionary = new Dictionary<string, HashSet<string>>();
			foreach (var assemblyFile in settings.Select(assemblyFiles))
			{
				var outEdges = AddOrGetNode(dictionary, assemblyFile);
				foreach (var reference in settings.Select(assemblyFile.References))
				{
					outEdges.Add(settings.GetName(reference));
				}
			}

			return dictionary.ToVertexAndEdgeListGraph(
				kv => Array.ConvertAll(kv.Value.ToArray(), v => new SEquatableEdge<string>(kv.Key, v))
				);
		}

		private HashSet<string> AddOrGetNode(Dictionary<string, HashSet<string>> dictionary, AssemblyFile assemblyFile)
		{
			HashSet<string> outEdges;
			string nodeName = settings.GetName(assemblyFile);
			if (!dictionary.TryGetValue(nodeName, out outEdges))
			{
				outEdges = new HashSet<string>();
				dictionary.Add(nodeName, outEdges);
			}
			return outEdges;
		}
	}
}
