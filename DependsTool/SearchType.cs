﻿using System;

namespace DependsTool
{
    [Flags]
    public enum SearchType
    {
        Assembly = 1,
        DefinedType = 2,
        ReferencedType = 4,
        Any = 7,
    }


}
